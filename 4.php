<?php
/**
 *	Closure:call()
 */

class Foo {
	private $foo = 'bar';
}

$getFooCallBack = function() {
	return $this->foo;
};

echo $getFooCallBack->call(new Foo);