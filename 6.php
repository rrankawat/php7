<?php

/**
 * Null Coalesce Operator
 */

$array = ['foo' => 'bar'];

// PHP5
echo $message = isset($array['foo']) ? $array['foo'] : 'not set';

// PHP7
echo $message = $array['foo'] ?? 'not set';